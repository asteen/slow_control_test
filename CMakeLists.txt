cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project( hexactrl )

include(CheckCXXCompilerFlag)
check_cxx_compiler_flag(-std=c++14 HAVE_FLAG_STD_CXX14)
if(HAVE_FLAG_STD_CXX14)
  set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14" )
  message("will use c++14")
else()
  check_cxx_compiler_flag(-std=c++11 HAVE_FLAG_STD_CXX11)
  if(HAVE_FLAG_STD_CXX11)
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11" )
    message("will use c++11")
  else()
    message( FATAL_ERROR "C++ 11 or higher is required")
  endif()
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fPIC -O3")

# find_package(Git)
# if(GIT_FOUND)
#   message("Git found ${GIT_EXECUTABLE}  ${GIT_VERSION_STRING} ")
# endif()

# if(NOT DEFINED BRANCH_NAME)
#   execute_process(
#     COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
#     WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
#     OUTPUT_VARIABLE BRANCH_NAME
#     OUTPUT_STRIP_TRAILING_WHITESPACE
#     )
# endif()
# message("DEFINE BRANCH_NAME ${BRANCH_NAME}")

# execute_process(
#   COMMAND git log -1 --format=%h
#   WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
#   OUTPUT_VARIABLE GIT_HASH
#   OUTPUT_STRIP_TRAILING_WHITESPACE
#   )
# set(DESTINATION "/opt/testSlowCtrl/${BRANCH_NAME}")
set(DESTINATION "/opt/testSlowCtrl/ ")

list( APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake )

find_package( Boost COMPONENTS thread system filesystem timer REQUIRED )

link_directories( ${EXTERN_BOOST_LIB_PREFIX} )

install(FILES ${PROJECT_SOURCE_DIR}/env.sh DESTINATION ${DESTINATION}/etc)
find_package( CACTUS REQUIRED )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/include  ${UHAL_LOG_INCLUDE_PREFIX} ${UHAL_GRAMMARS_INCLUDE_PREFIX} ${UHAL_UHAL_INCLUDE_PREFIX})
link_directories( ${link_directories} ${UHAL_LOG_LIB_PREFIX} ${UHAL_GRAMMARS_LIB_PREFIX} ${UHAL_UHAL_LIB_PREFIX} )

file( GLOB headers ${headers} ${PROJECT_SOURCE_DIR}/include/*.h )
file( GLOB executables ${PROJECT_SOURCE_DIR}/src/*.cxx )

message( "-- Executables to be installed : ${executables}")

foreach( exec ${executables} )
  message( "\t${exec}" )
  get_filename_component(ex ${exec} NAME_WE)
  add_executable( ${ex} ${exec} ${headers} )
  target_link_libraries( ${ex} ${Boost_LIBRARIES} boost_program_options boost_regex boost_serialization boost_chrono cactus_uhal_log cactus_uhal_grammars cactus_uhal_uhal )
  install( TARGETS ${ex} DESTINATION ${DESTINATION}/bin
    ARCHIVE DESTINATION ${DESTINATION}/lib
    LIBRARY DESTINATION ${DESTINATION}/lib )
endforeach()

string(TIMESTAMP TODAY "%Y.%m.%d.%H.%M.%S")

find_package(PythonInterp 3 REQUIRED)
include(FindPythonInterp)
set(PYTHON ${PYTHON_EXECUTABLE})
message(STATUS "\${PYTHON_EXECUTABLE} == ${PYTHON_EXECUTABLE}")

set(CPACK_RPM_BUILDREQUIRES python3-devel)
set(CPACK_RPM_SPEC_MORE_DEFINE "%define __python ${PYTHON_EXECUTABLE}")

set(CPACK_PACKAGE_VERSION ${TODAY})
set(CPACK_GENERATOR "RPM")
set(CPACK_PACKAGE_NAME "hexactrl-sw-${BRANCH_NAME}")
# set(CPACK_PACKAGE_RELEASE 1)
set(CPACK_PACKAGE_CONTACT "asteen")
set(CPACK_PACKAGE_VENDOR "hgcal")
set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${TODAY}-${GIT_HASH}.${CMAKE_SYSTEM_PROCESSOR}")

#configure_file("${CMAKE_CURRENT_SOURCE_DIR}/hexactrl_sw_centos.spec.in" "${CMAKE_CURRENT_BINARY_DIR}/hexactrl_sw_centos.spec" @ONLY)

#configure_file("${CMAKE_CURRENT_SOURCE_DIR}/hexactrl_sw_zynq.spec.in" "${CMAKE_CURRENT_BINARY_DIR}/hexactrl_sw_zynq.spec" @ONLY)

#set(CPACK_RPM_USER_BINARY_SPECFILE "${CMAKE_CURRENT_BINARY_DIR}/hexactrl_sw_zynq.spec")
include(CPack)
