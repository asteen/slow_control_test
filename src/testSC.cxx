#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/cstdint.hpp>
#include <boost/program_options.hpp>
#include <boost/timer/timer.hpp>

#include <uhal/uhal.hpp>


int main(int argc, char** argv)
{
  int m_uhalLogLevel;
  std::string m_connectionfile;
  try { 
    /** Define and parse the program options 
     */ 
    namespace po = boost::program_options; 
    po::options_description server_options("ZMQ server options"); 
    server_options.add_options()
      ("help,h", "Print help messages")
      ("connectionfile,f", po::value<std::string>(&m_connectionfile)->default_value("address_table/connection.xml"), "name of uhal connection file")
      ("uhalLogLevel,L", po::value<int>(&m_uhalLogLevel)->default_value(4), "uhal log level : 0-Disable; 1-Fatal; 2-Error; 3-Warning; 4-Notice; 5-Info; 6-Debug");
    //connection file as option; uhal device from configuration at initialize time
    po::options_description cmdline_options;
    cmdline_options.add(server_options);

    po::variables_map vm; 
    try { 
      po::store(po::parse_command_line(argc, argv, cmdline_options),  vm); 
      if ( vm.count("help")  ) { 
        std::cout << server_options   << std::endl; 
        return 0; 
      } 
      po::notify(vm);
    }
    catch(po::error& e) { 
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      std::cerr << server_options << std::endl; 
      return 1; 
    }
  }
  catch(std::exception& e) { 
    std::cerr << "Unhandled Exception reached the top of main: " 
              << e.what() << ", application will now exit" << std::endl; 
    return 2; 
  }   
  
  switch(m_uhalLogLevel){
  case 0:
    uhal::disableLogging();
    break;
  case 1:
    uhal::setLogLevelTo(uhal::Fatal());
    break;
  case 2:
    uhal::setLogLevelTo(uhal::Error());
    break;
  case 3:
    uhal::setLogLevelTo(uhal::Warning());
    break;
  case 4:
    uhal::setLogLevelTo(uhal::Notice());
    break;
  case 5:
    uhal::setLogLevelTo(uhal::Info());
    break;
  case 6:
    uhal::setLogLevelTo(uhal::Debug());
    break;
  }

  uhal::ConnectionManager manager( "file://" + m_connectionfile );
  uhal::HwInterface* m_uhalHW = new uhal::HwInterface( manager.getDevice("mylittlememory") );

  int nloops=10000;
  int nbr_SC_Transactions=5;
  double writetime = 0;
  double readtime = 0;
  double idletime = 0;
  double fulltime = 0;
  boost::timer::cpu_timer full_timer;
  boost::timer::cpu_timer local_timer;

  std::vector<uint32_t> data = { 0x00000006, 0x00000000, 0xE000FB01, 0x00010000,
				 0x03020100, 0x07060504, 0xE0000008, 0x00010000,
				 0x0B0A0908, 0x0F0E0D0C, 0xE0000808, 0x00010000,
				 0x00000000, 0x00000000, 0xE1000008, 0x00010000,
				 0x00000000, 0x00000000, 0xE1000808, 0x00010000 };
    
  
  full_timer.start();
  for( int loop=0; loop<nloops; loop++ ){
    local_timer.start();
    m_uhalHW->getNode("data.IC_TX_BRAM0.Data").writeBlock(data);
    m_uhalHW->getNode("config.IC_Control0.NbrTransactions").write(nbr_SC_Transactions);
    m_uhalHW->getNode("config.IC_Control0.Start").write(0x1);
    m_uhalHW->getNode("config.IC_Control0.Start").write(0x0);
    writetime += (double)local_timer.elapsed().wall/1e9;

    local_timer.start();
    while(1)
      if( m_uhalHW->getNode("config.IC_Status0.Busy").read() == 0 )
	break;
    idletime += (double)local_timer.elapsed().wall/1e9;
    
    local_timer.start();
    auto readvec = m_uhalHW->getNode("data.IC_RX_BRAM0.Data").readBlock(nbr_SC_Transactions*4);
    readtime += (double)local_timer.elapsed().wall/1e9;
  }
  fulltime = (double)full_timer.elapsed().wall/1e9;

  std::cout << "Full time  = " << fulltime  << std::endl;
  std::cout << "Write time = " << writetime << std::endl;
  std::cout << "Idle time  = " << idletime  << std::endl;
  std::cout << "Read time  = " << readtime  << std::endl;
  return 0;
}
