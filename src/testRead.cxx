#include <iostream>
#include <fstream>

#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/program_options.hpp>
#include <boost/cstdint.hpp>
#include <boost/timer/timer.hpp>

#include <iomanip>

int main(int argc, char *argv[])
{
  // using namespace boost::interprocess;
  namespace bip = boost::interprocess;

  std::string m_fileName;
  uint32_t m_address,m_nevents;
  uint32_t m_mapsize,m_nwords;
  try {
    /** Define and parse the program options                                                                                                                                                                        
     */
    namespace po = boost::program_options;
    po::options_description options("options");
    options.add_options()
      ("help,h", "Print help messages")
      ("fileName,f", po::value<std::string>(&m_fileName)->default_value("moduletest.raw"), "name of input file")
      ("address,a", po::value<uint32_t>(&m_address)->default_value(0x3000), "address in the memory mapped region")
      ("nevents,N", po::value<uint32_t>(&m_nevents)->default_value(10000), "number of times the readblock should be performed")
      ("mapsize,s", po::value<uint32_t>(&m_mapsize)->default_value(0x100000), "size in bytes to memory map")
      ("nwords,n", po::value<uint32_t>(&m_nwords)->default_value(0x1000), "number of words to be read in each readBlock");
    
    po::options_description cmdline_options;
    cmdline_options.add(options);

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, cmdline_options),  vm);
      if ( vm.count("help")  ) {
        std::cout << options   << std::endl;
        return 0;
      }
      po::notify(vm);
    }
    catch(po::error& e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << options << std::endl;
      return 1;
    }
    
    std::cout << std::endl;
    if( vm.count("fileName") )   std::cout << "fileName = "    << m_fileName << std::endl;
    if( vm.count("address") )    std::cout << "address = "     << m_address << std::endl;
    if( vm.count("mapsize") )    std::cout << "mapsize = "     << m_mapsize << std::endl;
    if( vm.count("nevents") )    std::cout << "nevents = "     << m_nevents << std::endl;
    if( vm.count("nwords") )     std::cout << "nwords = "      << m_nwords  << std::endl;
  }
  catch(std::exception& e) {
    std::cerr << "Unhandled Exception reached the top of main: "
	      << e.what() << ", application will now exit" << std::endl;
    return 2;
  }


  bip::mode_t themode = bip::read_write ;
  bip::file_mapping m_file(m_fileName.c_str(), themode);

  bip::mapped_region region(m_file,
			    themode,
			    0x0,
			    m_mapsize);

  //Get the address of the mapped region                                                                                                                                                                           
  void * addr       = region.get_address();
  uint32_t * data = (uint32_t *)static_cast<char*>(addr);

  std::vector<uint32_t> datavec;datavec.reserve(m_nwords);
    
  boost::timer::cpu_timer timer;
  timer.start();
  for( uint32_t ievt=0; ievt<m_nevents; ievt++ )
    std::memcpy(&datavec[0],&(data[m_address]),sizeof(uint32_t)*m_nwords);
  double readtime = (double)timer.elapsed().wall/1e9;

  std::for_each( datavec.begin(), datavec.end(), [&](auto &v){std::cout << std::hex << std::setw(8) << std::setfill('0') << v << "  ";} );

  std::cout << "Read time  = " << std::dec << readtime  << std::endl;

    
  return 0;
}

